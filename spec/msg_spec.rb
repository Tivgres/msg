RSpec.describe Msg do

  let(:default) {Msg.configure {}}
  let(:custom) {Msg.configure  do |config|
    config.message = 'different_message'
  end}

  it 'has a version number' do
    expect(Msg::VERSION).not_to be nil
  end

  it 'Testing default instance' do
    Msg.configure {}
    expect(Msg.configuration.message).to eq('Created by Tivgres')
  end

  it 'Testing output with default message' do
    expect { default }.to output("\"Created by Tivgres\"\n").to_stdout
  end

  it 'Testing set up instance' do
    Msg.configure do |config|
      config.message = 'different_message'
    end
    expect(Msg.configuration.message).to eq('different_message')
  end

  it 'Testing output with custom message' do
    expect { custom }.to output("\"different_message\"\n").to_stdout
  end
end
