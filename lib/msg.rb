require "msg/version"

module Msg
  class << self
    attr_accessor :configuration
  end

  def self.configure
    self.configuration ||= Configuration.new
    yield(configuration)
    p configuration.message Rails.const_defined?('Console') || Rails.const_defined?('Server')
  end

  class Configuration
    attr_accessor :message

    def initialize
      @message = 'Created by Tivgres'
    end
  end
end
