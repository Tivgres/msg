module Msg
  module Generators
    class InstallGenerator < Rails::Generators::Base
      source_root File.expand_path("../../templates", __FILE__)
      desc "Creates Msg initializer for your application"

      def copy_initializer
        template "msg_initializer.rb", "config/initializers/msg.rb"

        puts "Install complete! Truly Outrageous!"
      end
    end
  end
end